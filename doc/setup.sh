#!/usr/bin/env bash

# Set up Python venv
if [ -d "venv" ]; then
    rm -rf venv
fi

python3 -m venv venv
source venv/bin/activate
echo -e "Virtual ENV: ${VIRTUAL_ENV:?}"


# Install dependencies
python3 -m pip install --upgrade pip
python3 -m pip install -r requirements.txt

